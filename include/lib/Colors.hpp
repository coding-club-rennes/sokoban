/*
** Colors.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Wed Jan 28 15:20:37 2015 Alexis Bertholom
** Last update Wed Jan 28 15:25:08 2015 Alexis Bertholom
*/

#ifndef COLORS_HPP_
# define COLORS_HPP_

namespace	Colors
{
  enum		Color
    {
      Black =	0x000000,
      White =	0xFFFFFF,
      Red =	0xFF0000,
      Green =	0x00FF00,
      Blue =	0x0000FF,
      Yellow =	0xFFFF00,
      Cyan =	0x00FFFF,
      Magenta =	0xFF00FF
    };
}

#endif
