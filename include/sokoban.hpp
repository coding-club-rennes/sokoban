//
// sokoban.hpp for sokoban in /home/maudit/CodingClub/sokoban/include/
//
// Made by Thomas Couacault
// Login   <thomas.couacault@epitech.eu>
//
// Started on  Sun Jan  1 18:00:23 2017 Thomas Couacault
// Last update Jan Jan 8 14:27:29 2017
//

#ifndef SOKOBAN_HPP_
# define SOKOBAN_HPP_

# define WALL		      ('#')
# define FLOOR		      (' ')
# define PLAYER	      ('P')
# define STORE		      ('O')
# define BOX		      ('X')
# define RETURN_ERROR	(-1)

#include <iostream>
#include <vector>
using namespace std;

typedef  struct   s_map
{
   vector<string> buffer;
   unsigned int   width;
   unsigned int   height;
}                 t_map;

/*
** PARSING
*/
int	is_not_allowed(string);
void	config_map(t_map *, string);
void  check_errors(t_map *);
#endif
