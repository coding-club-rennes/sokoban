/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
** Last update Sun Jan  1 19:22:32 2017
*/

#include <stdio.h>
#include <unistd.h>
#include "SDLDisplay.hpp"
#include "Input.hpp"
#include "Colors.hpp"
#include "sokoban.hpp"

int		main(void)
{
  SDLDisplay	display("Sokoban", 800, 600);
  Input		input;
  t_map		map;

  config_map(&map, "levels/level_1");
  while (!(input.shouldExit()) && !(input.getKeyState(SDL_SCANCODE_ESCAPE)))
    {
      display.clearScreen();
      /* YOUR CODE GOES HERE */
      display.putRect(300, 200, 100, 50, Colors::Red);
      display.refreshScreen();
      input.flushEvents();
   }
  return (0);
}
