/*
** errors_parsing.c for my_sokoban in /home/maudit/EPITECH/PSU_2016_my_sokoban/parsing/
**
** Made by Thomas Couacault
** Login   <thomas.couacault@epitech.eu>
**
** Started on  Tue Dec 13 15:38:26 2016 Thomas Couacault
// Last update Sun Jan  8 14:28:22 2017 Thomas Couacault
*/

#include "sokoban.hpp"

int   is_not_allowed(string str)
{
   unsigned int   i;

   i = 0;
   while (i < str.size())
   {
      if (str[i] != WALL && str[i] != FLOOR
         && str[i] != PLAYER && str[i] != STORE
         && str[i] != BOX && str[i] != '\n')
      return (i + 1);
      i++;
   }
   return (0);
}

void   check_errors(t_map *map)
{
   unsigned int   i;
   unsigned int   index;

   i = 0;
   while (i < map->height)
   {
      if ((index = is_not_allowed(map->buffer[i])))
      {
         cerr << "Sokoban: Character not allowed in map file."
         << " [" << i << "][" << index - 1 << "]" << endl;
         exit(1);
      }
      if (map->width != map->buffer.size())
      {
         cerr << "Sokoban: Map is not a rectangle." << endl;
         exit(1);
      }
      i++;
   }
}
