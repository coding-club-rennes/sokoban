/*
** save_map.c for my_sokoban in /home/maudit/EPITECH/PSU_2016_my_sokoban/parsing/
**
** Made by Thomas Couacault
** Login   <thomas.couacault@epitech.eu>
**
** Started on  Mon Dec 12 09:32:53 2016 Thomas Couacault
// Last update Sun Jan  8 14:22:57 2017 Thomas Couacault
*/

#include <fstream>
#include "sokoban.hpp"

void  config_map(t_map *map, string file_path)
{
   ifstream level(file_path);
   string line;

   map->height = 0;
   if(level)
   {
      while(getline(level, line))
      {
         map->buffer.push_back(line);
         map->height += 1;
      }
      map->width = map->buffer[0].size() - 1;
   }
   else
      cerr << "Sokoban: level file not found." << endl;
   check_errors(map);
}
