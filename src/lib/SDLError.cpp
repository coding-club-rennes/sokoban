//
// SDLError.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 19:21:29 2014 bertho_d
// Last update Mon Aug  4 01:20:29 2014 bertho_d
//

#include <SDL2/SDL.h>
#include "SDLError.hpp"

SDLError::SDLError() : Error("SDL error")
{
  this->addErrorSuffix(SDL_GetError());
}

SDLError::SDLError(const char *message) : Error("SDL error")
{
  this->addErrorSuffix(message);
  this->addErrorSuffix(SDL_GetError());
}
