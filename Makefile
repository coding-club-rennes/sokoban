##
## Makefile for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
##
## Made by bertho_d
## Login   <bertho_d@epitech.net>
##
## Started on  Fri Jul 25 03:24:57 2014 bertho_d
## Last update Sun Jan  1 19:12:09 2017 
##

NAME		= sokoban

SRCDIR		= src/
INCLDIR		= include/
LIBDIR		= lib/

SRC		= $(SRCDIR)main.cpp \
		  $(SRCDIR)$(LIBDIR)DevError.cpp \
		  $(SRCDIR)$(LIBDIR)Error.cpp \
		  $(SRCDIR)$(LIBDIR)FileError.cpp \
		  $(SRCDIR)$(LIBDIR)GenericError.cpp \
		  $(SRCDIR)$(LIBDIR)Image.cpp \
		  $(SRCDIR)$(LIBDIR)Input.cpp \
		  $(SRCDIR)$(LIBDIR)SDLContext.cpp \
		  $(SRCDIR)$(LIBDIR)SDLDisplay.cpp \
		  $(SRCDIR)$(LIBDIR)SDLError.cpp   \
		  $(SRCDIR)parsing/map.cpp	\
		  $(SRCDIR)parsing/errors.cpp

OBJ		= $(SRC:.cpp=.o)

CXXFLAGS		+= -O3
CXXFLAGS		+= -pedantic
CXXFLAGS		+= -W
CXXFLAGS		+= -Wall
CXXFLAGS		+= -Wextra
CXXFLAGS		+= -std=c++11
CXXFLAGS		+= -I$(INCLDIR)
CXXFLAGS		+= -I$(INCLDIR)$(LIBDIR)

LIBS		+= -lSDL2
LIBS		+= -lSDL2_image
LIBS		+= -lSDL2_ttf
# LIBS		+= -lSDL2main

CXX		= g++
RM		= rm -f

all: $(NAME)

$(NAME): $(OBJ)
	$(CXX) $(OBJ) $(LIBS) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
